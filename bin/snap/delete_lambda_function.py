import boto3

def delete_lambda_function(arn):
    """
    create client connection and delete aws lambda function resource
    :param arn: string arn resource of aws lambda function
    :return: response of deleting the function
    """
    client = boto3.client('lambda')
    response = client.delete_function(
        FunctionName=arn,
    )
    return response
