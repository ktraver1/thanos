Thanos is service that monitors and deletes resources

Features
    Destroy all resources
    Scan all resources
    Scan new resources
    Read json template for rules
    Create json with prompts
    Destroy all resources that are not following rules
    Monitor all new resources and destroy or alert if not correct
    
